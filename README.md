# Application Blueprint for Discourse

## What is Discourse?
Discourse is the 100% open source discussion platform built for the next decade of the Internet. Use it as a mailing list, discussion forum, long-form chat room, and more!

**Documentation at:** <https://hub.docker.com/r/bitnami/discourse>

This Application Blueprint is made possible by the [Community Maintained One Click Apps repository](https://github.com/caprover/one-click-apps).
If there are any apps you would like to help find their place in our Free and Open Cloud, consider contributing to One-Click Apps as well.
For more information on CapRover or their one One-Click Apps platform, visit [caprover.com](https://caprover.com/docs/one-click-apps.html).

`ensemble-generated.yaml` was generated from [https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/discourse.yml](https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/discourse.yml).  Changes can instead be applied to `ensemble-template.yaml` to overwrite the generated values.
